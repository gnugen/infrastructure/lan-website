# Site Web LAN

Ce site statique sert à annoncer quel à quel jeu nous jouons lors d'une
LAN/WAN; pour permettre à ceux qui le souhaitent, ou qui ne peuvent pas
suivre par d'autre moyens, de savoir où nous trouver. Une instance est
hébergée sur rainbowdash: https://gnugeneration.epfl.ch/games/


## Compilation

```sh
make
```

`ls *.html`.

## Utilisation

```
#(ssh rainbowdash)
#(sudo -u www-data)
cd /var/www/games
# on passe à wesnoth
./playing wesnoth
# on passe à teeworlds
./playing teeworlds
# on a fini
./playing game-list
```

## Disclaimer

Ce bout de php est probablement le pire code que j'ai écrit ces 5 dernières
années. Il a été écrit pendant un semestre chargé, expédié en une journée
et il ne contient aucune abstraction; pas même un fonction! Le html produit,
en revanche, est décent. Même s'il est mal indenté.

## License

© Antoine Fontaine <antoine.fontaine@epfl.ch>, GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The files in the `games/` directory are not my creation.
