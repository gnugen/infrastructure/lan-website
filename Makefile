
all: game-pages game-list.html

.PHONY: game-pages
game-pages: wesnoth.html unvanquished.html minetest.html teeworlds.html

%.html: games.php
	php -f games.php -- $* > $*.html

game-list.html: games.php
	php -f games.php > game-list.html

clean:
	rm *.html
