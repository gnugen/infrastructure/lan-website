<?php
# SPDX-License-Identifier: GPL-3.0-or-later
#
if (!isset($game)) {
	die("this file is not meant to be accessed directly.");
}
?>
<div class="get-the-game">
<?php
switch ($game) {
case "unvanquished": ?>
	Grab the game from
	<a href="https://unvanquished.net/download/">here</a>
	— Join the Gnugen server from the list or use <a href="unv://gnugen.ch:27960">this link</a>
<?php break;
case "wesnoth": ?>
	<p>
		<code>sudo apt install wesnoth-1.14</code><br>or<br>
		<a href="https://wesnoth.org/#download">
			get it from the website
		</a>
	</p><p>
		then connect wesnoth to the <code>gnugen.ch</code> server
	</p>
<?php break;
case "teeworlds": ?>
	<p>
		<code>sudo apt install teeworlds</code><br>or<br>
		<a href="https://teeworlds.com/?page=downloads">
			get the game from the website
		</a>
	</p><p>
		then connect teeworlds to the <code>gnugen.ch</code>
		server
	</p>
<?php break;
case "minetest": ?>
	<p>
		<code>sudo apt install minetest</code><br>or<br>
		<a href="https://www.minetest.net/downloads/">
			get it from the website
		</a>
	</p><p>
		Connect minetest to the <code>gnugen.ch</code> server, on
		port 30000 for an easy world with few mods, or port 30001 for a lot of mods and a bit more of a challenge 
	</p>
<?php break;
}
?>
</div>
