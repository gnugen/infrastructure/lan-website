<?php
# SPDX-License-Identifier: GPL-3.0-or-later

$existing_games = [
	"teeworlds" => "Teeworlds",
	"wesnoth" => "Battle for Wesnoth",
	"minetest" => "Minetest",
	"unvanquished" => "Unvanquished",
];

if ($argc > 2) {
	die("too many arguments");
}
if ($argc == 2) {
	if ($argv[1] == "-h" or $argv[1] == "--help") {
		echo "usage: ${argv[0]} [current-game]\n"
		   . "if no game is specified, this will generate the game list.";
	}
	$game = $argv[1];
	$playing = true;
	$game_dispname = $existing_games[$game];
} else {
	$playing = false;
}

if ($playing and !in_array($game, array_keys($existing_games))) {
	die("game must be an existing game. Existing games are "
		. implode(", ", array_keys($existing_games)). ".");
}

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="icon" href="/assets/images/gnugen.svg" />
	<title><?php if ($playing) echo "Playing ". $game_dispname ." — "; ?>Gnugen's LAN</title>
	<link rel="stylesheet" href="styles.css" />
</head>
<body>
<?php
	if ($playing) {
?>
	<main class="game" id="game-<?php echo $game; ?>">
	<div class="page">
		<div class="box" id="currently-playing">
			We are currently Playing
			<strong><?php echo $game_dispname; ?></strong>
		</div>
<?php if ($game == "teeworlds") { ?>
		<table><td>
			<img class="splashtee" alt=""
			     src="games/teeworlds/splashtee6.png" />
		</td><td>
			<img class="logo"
			     src="games/<?php echo $game; ?>/logo.png" />
		</td></table>
<?php } else { ?>
		<div>
			<img class="logo"
			     src="games/<?php echo $game; ?>/logo.png" />
		</div>
<?php } ?>
		<div class="box" id="join">
		<?php
			include("join.php");
		?>
		</div>
                <div class="box">
                    <p>
                        You can use our <a href="https://wiki.gnugen.ch/association/mumble">Mumble server</a> to talk freely with other participants during the event
                    </p>
                </div>
	</div>
	</main>
<?php
	} else { // if !playing
?>
	<main id="not-playing">
	<div class="page">
		<div class="box">
			<h1>The LAN is now finished, thanks to all who played</h1>
			<h2>The LAN took place the 6th of december.</h2>
                </div>
                <div class="box">
                    <p>
                        If you already want to connect to our servers to test or practice, or if you just want to see the list of games, you can look at our
                        <a href="https://wiki.gnugen.ch/association/games"> 
                        wiki entry about our game servers
                        </a>
                    </p>
                    <!--
                    <p>
                        You can use our <a href="https://wiki.gnugen.ch/association/mumble">Mumble server</a> to talk freely with other participants during the event
                    </p>
                    -->
                </div>
		<a href="https://gnugen.ch" class="gnugen-logo">GNU Generation</a>

		<img src="games/banner.png" />
	</div>
	</main>
<?php
	}
?>
</body>
</html>
